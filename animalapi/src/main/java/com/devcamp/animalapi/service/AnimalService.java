package com.devcamp.animalapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.animalapi.models.Cat;
import com.devcamp.animalapi.models.Dog;

@Service
public class AnimalService {
    Cat cat1 = new Cat("Kitty");
    Cat cat2 = new Cat("Keit");
    Cat cat3 = new Cat("Lucky");
    Dog dog1 = new Dog("Doggy");
    Dog dog2 = new Dog("Puffy");
    Dog dog3 = new Dog("Luci");
    public ArrayList<Cat> getAllCats(){
        ArrayList<Cat> allCatList = new ArrayList<>();
        allCatList.add(cat1);
        allCatList.add(cat2);
        allCatList.add(cat3);
        return allCatList;
    }

    public ArrayList<Dog> getAllDogs(){
        ArrayList<Dog> allDogList = new ArrayList<>();
        allDogList.add(dog1);
        allDogList.add(dog2);
        allDogList.add(dog3);
        return allDogList;

    }
}
